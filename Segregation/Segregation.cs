﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Segregation
{
    public class Segregation
    {
        /// <summary>
        /// Кубическая кристаллическая решётка
        /// </summary>
        public Cell[,,] Structure;
        /// <summary>
        /// Потенциал Терсоффа
        /// </summary>
        private Tersoff TRS = new Tersoff();
        /// <summary>
        /// Потенциал Стиллинджера-Вебера
        /// </summary>
        private StillingerWeber STW = new StillingerWeber();

        /// <summary>
        /// Создание кубической структуры
        /// </summary>
        /// <param name="length">Размер структуры</param>
        public Segregation(int length, double gePart, Random rand)
        {
            Structure = new Cell[length, length, length];

            int countAtoms = length * length * length * Cell.CountAtoms;
            int geKol = (int)(gePart * countAtoms);

            Atom.LatPar = 0.54307 * (1 - gePart) + 0.5660 * gePart;

            for (int x = 0; x < length; x++)
                for (int y = 0; y < length; y++)
                    for (int z = 0; z < length; z++)
                    {
                        Structure[x, y, z] = new Cell(x, y, z);
                    }

            int count = 0;
            while (true)
            {
                if (count == geKol) return;
                for (int x = 0; x < length; x++)
                    for (int y = 0; y < length; y++)
                        for (int z = 0; z < length; z++)
                            for (int temp = 0; temp < Cell.CountAtoms; temp++)
                            {
                                double res = rand.Next(0, 2);
                                if (res == 1)
                                {
                                    Structure[x, y, z].atoms[temp].atomType = AtomType.Ge;
                                    count++;
                                }
                                if (count == geKol) return;
                            }
            }
        }

        /// <summary>
        /// Поиск соседей выбранного атома
        /// </summary>
        /// <param name="selAtom">Атом, для которого ищутся соседи</param>
        /// <param name="indexSelAtom">Индекс атома в ячейке</param>
        /// <param name="radiusCutoff">Радиус отсекания соседей</param>
        /// <param name="atomTypeNeighbours">Тип соседей выбранного атома</param>
        /// <returns></returns>
        private Atom[] SearchNeighbours(Atom selAtom, int indexSelAtom, double radiusCutoff)
        {
            List<Atom> atomNeighbours = new List<Atom>();

            int length = Structure.GetLength(0);
            int x = selAtom.cellCoord.GetIntX;
            int y = selAtom.cellCoord.GetIntY;
            int z = selAtom.cellCoord.GetIntZ;

            for (int lx = -1; lx <= 1; lx++)
                for (int ly = -1; ly <= 1; ly++)
                    for (int lz = -1; lz <= 1; lz++)//Перебор соседей, где lx, ly, lz - шаги
                    {
                        int posCellNB_X = x + lx, posCellNB_Y = y + ly, posCellNB_Z = z + lz;//Позиция ячейки-соседа
                        Vector additive = new Vector(0, 0, 0);//Добавка для периодических граничных условий

                        /*
                         * Если выбранная ячейка (x,y,z) на границе, а ячейка-сосед (coordX, coordY, coordZ) за границей,
                         * то берём ячейку в конце структуры и с помощью добавки переносим в нужную позицию (lx, ly, lz)
                         */

                        if (x == 0 && lx == -1)
                        {
                            posCellNB_X = length - 1;
                            additive.x = -length;
                        }
                        if (x == length - 1 && lx == 1)
                        {
                            posCellNB_X = 0;
                            additive.x = length;
                        }

                        if (y == 0 && ly == -1)
                        {
                            posCellNB_Y = length - 1;
                            additive.y = -length;
                        }
                        if (y == length - 1 && ly == 1)
                        {
                            posCellNB_Y = 0;
                            additive.y = length;
                        }

                        if (z == 0 && lz == -1)
                        {
                            posCellNB_Z = length - 1;
                            additive.z = -length;
                        }
                        if (z == length - 1 && lz == 1)
                        {
                            posCellNB_Z = 0;
                            additive.z = length;
                        }

                        for (int a = 0; a < Cell.CountAtoms; a++)//Перебор атомов в ячейке-соседе
                        {
                            //Чтобы не брать уже выбранный атом
                            if (x == posCellNB_X && y == posCellNB_Y && z == posCellNB_Z && a == indexSelAtom) continue;

                            //Атом-сосед
                            Atom atomNeighbour = Structure[posCellNB_X, posCellNB_Y, posCellNB_Z].atoms[a];
                            atomNeighbour.SetCoord = atomNeighbour.GlobalCoord + additive;
                            double radius = Vector.Magnitude(atomNeighbour.AtomCoord, Structure[x, y, z].atoms[indexSelAtom].GlobalCoord);

                            if (radius <= radiusCutoff)
                            {
                                atomNeighbours.Add(atomNeighbour);
                            }
                        }
                    }

            return atomNeighbours.ToArray();
        }


        /// <summary>
        /// Подсчёт полной потенциальной энергии системы
        /// </summary>
        /// <param name="tersoff">С потенциалом Терсоффа, либо Стиллинджера-Вебера</param>
        /// <returns></returns>
        public double SumEnergy(bool tersoff)
        {
            double sumEnergy = 0;
            int length = Structure.GetLength(0);


            if (tersoff)
            {
                for (int x = 0; x < length; x++)
                    for (int y = 0; y < length; y++)
                        for (int z = 0; z < length; z++)//Перебор ячеек структуры
                            for (int i = 0; i < Cell.CountAtoms; i++)//Перебор атомов в ячейке
                            {
                                Atom selAtom = Structure[x, y, z].atoms[i];
                                //Получаем соседей
                                Atom[] atomNeighbours = SearchNeighbours(selAtom, i, Atom.LatPar);

                                sumEnergy += TRS.PotentialEnergy(selAtom, i, ref atomNeighbours);
                            }
            }
            else
            {
                for (int x = 0; x < length; x++)
                    for (int y = 0; y < length; y++)
                        for (int z = 0; z < length; z++)//Перебор ячеек структуры
                            for (int i = 0; i < Cell.CountAtoms; i++)//Перебор атомов в ячейке
                            {
                                Atom selAtom = Structure[x, y, z].atoms[i];
                                //Получаем соседей
                                Atom[] atomNeighbours = SearchNeighbours(selAtom, i, Atom.LatPar / 2);

                                sumEnergy += STW.PotentialEnergy(selAtom, i, ref atomNeighbours);
                            }
            }
            return sumEnergy;
        }
        //Алгоритм Верле
        private void AlgorithmVerlet(ref Atom atom, Vector posAtom, Vector posCell, double stepTime)
        {
            Vector speedup = new Vector();//An
            Vector coord = atom.AtomCoord + atom.atomVeloc * stepTime + speedup * Math.Pow(stepTime, 2) / 2;

            speedup += speedup;//An+1

            Vector veloc = atom.atomVeloc + speedup * stepTime / 2;

            atom.AtomCoord = coord;
            atom.atomVeloc = veloc;
        }


    }
}