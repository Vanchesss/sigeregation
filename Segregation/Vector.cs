﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Segregation
{
    /*
     * Вспомогательная структура "Vector" - хранит три пространственные координаты (x, y, z), т.е. это ТОЧКА в пространстве
     */
    public struct Vector
    {
        /// <summary>
        /// Координата X
        /// </summary>
        public double x;
        /// <summary>
        /// Координата Y
        /// </summary>
        public double y;
        /// <summary>
        /// Координата Z
        /// </summary>
        public double z;

        /// <summary>
        /// Возвращает преобразованную в int координату X 
        /// </summary>
        public int GetIntX
        {
            get { return (int)x; }
        }
        /// <summary>
        /// Возвращает преобразованную в int координату Y
        /// </summary>
        public int GetIntY
        {
            get { return (int)y; }
        }
        /// <summary>
        /// Возвращает преобразованную в int координату Z 
        /// </summary>
        public int GetIntZ
        {
            get { return (int)z; }
        }

        /// <summary>
        /// Создание вектора
        /// </summary>
        /// <param name="x">Координата X</param>
        /// <param name="y">Координата Y</param>
        /// <param name="z">Координата Z</param>
        public Vector(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        /// <summary>
        /// Длина вектора. Начальная точка - текущие координаты
        /// </summary>
        /// <param name="vector">Конечная точка</param>
        /// <returns></returns>
        public double Magnitude(Vector vector)
        {
            return Math.Sqrt(Math.Pow(vector.x - x, 2) + Math.Pow(vector.y - y, 2) + Math.Pow(vector.z - z, 2));
        }
        /// <summary>
        /// Длина вектора
        /// </summary>
        /// <param name="vector1">Начальная точка</param>
        /// <param name="vector2">Конечная точка</param>
        /// <returns></returns>
        public static double Magnitude(Vector vector1, Vector vector2)
        {
            return Math.Sqrt(Math.Pow(vector2.x - vector1.x, 2) + Math.Pow(vector2.y - vector1.y, 2) + Math.Pow(vector2.z - vector1.z, 2));
        }
        /// <summary>
        /// Возвращает значение наибольшей координаты
        /// </summary>
        /// <returns></returns>
        public double MaxElement()
        {
            if (x >= y && x >= z) return x;
            else if (y >= x && y >= z) return y;
            else return z;
        }
        /// <summary>
        /// Возвращает значение наименьшей координаты
        /// </summary>
        /// <returns></returns>
        public double MinElemnt()
        {
            if (x <= y && x <= z) return x;
            else if (y <= x && y <= z) return y;
            else return z;
        }

        // |Операции между точками (x, y, z)|
        public static Vector operator +(Vector vec1, Vector vec2)
        {
            return new Vector(vec1.x + vec2.x, vec1.y + vec2.y, vec1.z + vec2.z);
        }
        public static Vector operator -(Vector vec1, Vector vec2)
        {
            return new Vector(vec1.x - vec2.x, vec1.y - vec2.y, vec1.z - vec2.z);
        }
        public static Vector operator *(Vector vec1, Vector vec2)
        {
            return new Vector(vec1.x * vec2.x, vec1.y * vec2.y, vec1.z * vec2.z);
        }
        public static Vector operator /(Vector vec1, Vector vec2)
        {
            return new Vector(vec1.x / vec2.x, vec1.y / vec2.y, vec1.z / vec2.z);
        }
        // |Операции между точкой (x, y, z) и числом|
        //Умножение числа на точку (x, y, z) справа
        public static Vector operator *(Vector vec, double num)
        {
            return new Vector(vec.x * num, vec.y * num, vec.z * num);
        }
        public static Vector operator *(Vector vec, int num)
        {
            return new Vector(vec.x * num, vec.y * num, vec.z * num);
        }
        //Умножение числа на точку (x, y, z) слева
        public static Vector operator *(double num, Vector vec)
        {
            return new Vector(vec.x * num, vec.y * num, vec.z * num);
        }
        public static Vector operator *(int num, Vector vec)
        {
            return new Vector(vec.x * num, vec.y * num, vec.z * num);
        }
        //Деление точки (x, y, z) на число
        public static Vector operator /(Vector vec, double num)
        {
            return new Vector(vec.x / num, vec.y / num, vec.z / num);
        }
        public static Vector operator /(Vector vec, int num)
        {
            return new Vector(vec.x / num, vec.y / num, vec.z / num);
        }
    }
}