﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Segregation
{
    public class Cell
    {
        /// <summary>
        /// Количество атомов в ячейке
        /// </summary>
        public const int CountAtoms = 8;

        //Атомы в ячейке
        public Atom[] atoms = new Atom[CountAtoms];

        /// <summary>
        /// Создание ячейки
        /// </summary>
        /// <param name="x">Позиция ячейки по X</param>
        /// <param name="y">Позиция ячейки по Y</param>
        /// <param name="z">Позиция ячейки по Z</param>
        public Cell(int x, int y, int z)
        {
            double koff = 0.25d;
            koff *= Atom.LatPar;

            atoms[0].AtomCoord = new Vector(0, 0, 0) * koff;
            atoms[1].AtomCoord = new Vector(0, 2, 2) * koff;
            atoms[2].AtomCoord = new Vector(2, 0, 2) * koff;
            atoms[3].AtomCoord = new Vector(2, 2, 0) * koff;

            atoms[4].AtomCoord = new Vector(1, 1, 1) * koff;
            atoms[5].AtomCoord = new Vector(1, 3, 3) * koff;
            atoms[6].AtomCoord = new Vector(3, 1, 3) * koff;
            atoms[7].AtomCoord = new Vector(3, 3, 1) * koff;

            //Задаём атомам позицию ячейки, в которой они находятся
            for (int i = 0; i < Cell.CountAtoms; i++)
            {
                atoms[i].cellCoord = new Vector(x, y, z);
            }
        }
        //a = asi*(1-x) + age*x
        /// <summary>
        /// Создание ячейки, в которой тип каждого атома определяется случайным образом
        /// </summary>
        /// <param name="x">Позиция ячейки по X</param>
        /// <param name="y">Позиция ячейки по Y</param>
        /// <param name="z">Позиция ячейки по Z</param>
        /// <param name="rand">Генератор случайных чисел</param>
        public Cell(int x, int y, int z, Random rand)
        {
            double koff = 0.25d;
            koff *= Atom.LatPar;

            atoms[0].AtomCoord = new Vector(0, 0, 0) * koff;
            atoms[1].AtomCoord = new Vector(0, 2, 2) * koff;
            atoms[2].AtomCoord = new Vector(2, 0, 2) * koff;
            atoms[3].AtomCoord = new Vector(2, 2, 0) * koff;

            atoms[4].AtomCoord = new Vector(1, 1, 1) * koff;
            atoms[5].AtomCoord = new Vector(1, 3, 3) * koff;
            atoms[6].AtomCoord = new Vector(3, 1, 3) * koff;
            atoms[7].AtomCoord = new Vector(3, 3, 1) * koff;

            //Задаём атомам тип и позицию ячейки, в которой они находятся
            for (int i = 0; i < Cell.CountAtoms; i++)
            {
                atoms[i].atomType = (rand.Next(0, 2) == 0 ? AtomType.Si : AtomType.Ge);
                atoms[i].cellCoord = new Vector(x, y, z);
            }
        }
    }
}
