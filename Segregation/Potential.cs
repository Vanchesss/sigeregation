﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Segregation
{
    //Абстрактный класс для потенциалов - для каждого можно вызвать одинаковый метод
    public abstract class Potential
    {
        public abstract double PotentialEnergy(Atom selAtom, int indexSelAtom, ref Atom[] atomNeighbours);
    }

    public class Tersoff : Potential
    {
        //Параметры потенциала Терсоффа
        private struct ParamPotential
        {
            public double A, B, D, R, b, c, d, n, h, l1, l2, l3;

            public ParamPotential(double A, double B, double D, double R, double b, double c, double d, double n, double h, double l1, double l2, double l3)
            {
                this.A = A;
                this.B = B;
                this.D = D;
                this.R = R;
                this.b = b;
                this.c = c;
                this.d = d;
                this.n = n;
                this.h = h;
                this.l1 = l1;
                this.l2 = l2;
                this.l3 = l3;
            }
            //Используется для получения параметров SiGe
            public ParamPotential(ParamPotential potential1, ParamPotential potential2)
            {
                this.A = Math.Sqrt(potential1.A * potential2.A);
                this.B = Math.Sqrt(potential1.B * potential2.B);
                this.D = Math.Sqrt(potential1.D * potential2.D);
                this.R = Math.Sqrt(potential1.R * potential2.R);
                this.b = Math.Sqrt(potential1.b * potential2.b);
                this.c = Math.Sqrt(potential1.c * potential2.c);
                this.d = Math.Sqrt(potential1.d * potential2.d);
                this.n = Math.Sqrt(potential1.n * potential2.n);
                this.h = Math.Sqrt(potential1.h * potential2.h);
                this.l1 = 0.5 * (potential1.l1 + potential2.l1);
                this.l2 = 0.5 * (potential1.l2 + potential2.l2);
                this.l3 = 0.5 * (potential1.l3 + potential2.l3);
            }
        }
        //Параметры для различных типов атомов
        private ParamPotential TRS_Si, TRS_Ge, TRS_SiGe;

        public Tersoff()
        {
            TRS_Si = new ParamPotential(1830.8, 471.18, 0.03, 0.34, 1.1e-6, 100390, 16.217, -0.59825, 0.78734, 2.4799, 1.7322, 0);//0.015 0.285
            TRS_Ge = new ParamPotential(1769, 419.23, 0.06, 0.32, 9.0166e-7, 106430, 15.652, -0.43884, 0.75627, 2.4451, 1.7047, 0);//0.03 0.28
            TRS_SiGe = new ParamPotential(TRS_Si, TRS_Ge);//0.021 0.282
        }

        /// <summary>
        /// Функция обрезания Fc
        /// </summary>
        /// <param name="potential">Параметры потенциала</param>
        /// <param name="radius">Расстояние до атома-соседа</param>
        /// <returns></returns>
        private double P_FunctionCutoff(ParamPotential potential, double radius)
        {
            if (radius < (potential.R - potential.D)) return 1;
            else if (radius > (potential.R + potential.D)) return 0;
            else return 0.5 * (1 - Math.Sin(Math.PI * (radius - potential.R) / (2 * potential.D)));
        }
        /// <summary>
        /// Функция притяжения Fa(r)
        /// </summary>
        /// <param name="potential">Параметры потенциала</param>
        /// <param name="Rij">Расстояние до атома-соседа</param>
        /// <returns></returns>
        private double P_FunctionA(ParamPotential potential, double Rij)
        {
            return potential.A * Math.Exp(-potential.l1 * Rij);
        }
        /// <summary>
        /// Функция отталкивания Fr(r)
        /// </summary>
        /// <param name="potential">Параметры потенциала</param>
        /// <param name="Rij">Расстояние до атома-соседа</param>
        /// <returns></returns>
        private double P_FunctionB(ParamPotential potential, double Rij)
        {
            return -potential.B * Math.Exp(-potential.l2 * Rij);
        }
        /// <summary>
        /// Функция G(O)
        /// </summary>
        /// <param name="potential">Параметры потенциала</param>
        /// <param name="Rij">Расстояние до первого атома-соседа</param>
        /// <param name="Rik">Расстояние до второго атома-соседа</param>
        /// <param name="Rjk">Расстояние между первым атомом-соседом и вторым атомом-соседом</param>
        /// <returns></returns>
        private double P_FunctionG(ParamPotential potential, double Rij, double Rik, double Rjk)
        {
            double cosTeta = (Math.Pow(Rik, 2) + Math.Pow(Rij, 2) - Math.Pow(Rjk, 2)) / (2 * Rij * Rik);
            double c2 = Math.Pow(potential.c, 2);//с^2
            double d2 = Math.Pow(potential.d, 2);//d^2

            return 1 + (c2 / d2) - (c2 / (d2 + Math.Pow(potential.h - cosTeta, 2)));
        }
        /// <summary>
        /// Функция Дзета_ij
        /// </summary>
        /// <param name="potential">Параметры потенциала</param>
        /// <param name="selAtom">Выбранный атом (i)</param>
        /// <param name="atomNeighbours">Атомы-соседи</param>
        /// <param name="indexNeighbour">Индекс в массиве соседей первого атома-соседа (j)</param>
        /// <returns></returns>
        private double P_FunctionDzeta(ParamPotential potential, Atom selAtom, ref Atom[] atomNeighbours, int indexNeighbour)
        {
            double sum = 0;
            double Rij = Vector.Magnitude(selAtom.GlobalCoord, atomNeighbours[indexNeighbour].AtomCoord);

            for (int k = 0; k < atomNeighbours.Length; k++)
            {
                if (k == indexNeighbour) continue;
                double Rik = Vector.Magnitude(selAtom.GlobalCoord, atomNeighbours[k].AtomCoord);
                double Rjk = Vector.Magnitude(atomNeighbours[indexNeighbour].AtomCoord, atomNeighbours[k].AtomCoord);

                double exponenta = potential.l3 == 0 ? 1 : Math.Exp(Math.Pow(potential.l3, 3) * Math.Pow(Rij - Rik, 3));
                sum += P_FunctionCutoff(potential, Rik) * P_FunctionG(potential, Rij, Rik, Rjk) * exponenta;
            }

            return sum;
        }
        /// <summary>
        /// Функция bij
        /// </summary>
        /// <param name="potential">Параметры потенциала</param>
        /// <param name="selAtom">Выбранный атом (i)</param>
        /// <param name="atomNeighbours">Атомы-соседи</param>
        /// <param name="indexNeighbour">Индекс в массиве соседей первого атома-соседа (j)</param>
        /// <returns></returns>
        private double P_FunctionBeta(ParamPotential potential, Atom selAtom, ref Atom[] atomNeighbours, int indexNeighbour)
        {
            double dzeta = P_FunctionDzeta(potential, selAtom, ref atomNeighbours, indexNeighbour);

            if (dzeta == 0 && potential.n < 0) return 0;
            return Math.Pow(1 + Math.Pow(potential.b * dzeta, potential.n), -1 / (2 * potential.n));
        }
        /// <summary>
        /// Потенциальная энергия выбранного атома
        /// </summary>
        /// <param name="selAtom">Выбранный атом</param>
        /// <param name="indexSelAtom">Индекс выбранного атома в ячейке</param>
        /// <param name="atomNeighbours">Атомы-соседи</param>
        /// <param name="atomTypeNeigbours">Типы атомов-соседей</param>
        /// <returns></returns>
        public override double PotentialEnergy(Atom selAtom, int indexSelAtom, ref Atom[] atomNeighbours)
        {
            double atomEnergy = 0;

            for (int j = 0; j < atomNeighbours.Length; j++)
            {
                double Rij = Vector.Magnitude(selAtom.GlobalCoord, atomNeighbours[j].AtomCoord);

                //В зависимости от типов атомов меняются параметры потенциала
                ParamPotential potential = TRS_SiGe;
                if (selAtom.atomType == AtomType.Si && selAtom.atomType == atomNeighbours[j].atomType) potential = TRS_Si;
                if (selAtom.atomType == AtomType.Ge && selAtom.atomType == atomNeighbours[j].atomType) potential = TRS_Ge;

                double beta = P_FunctionBeta(potential, selAtom, ref atomNeighbours, j);
                double Vij = P_FunctionCutoff(potential, Rij) * (P_FunctionA(potential, Rij) + beta * P_FunctionB(potential, Rij));

                atomEnergy += Vij;
            }

            atomEnergy /= 2d;
            return atomEnergy;
        }
    }

    public class StillingerWeber : Potential
    {
        //Параметры потенциала Стиллинджера-Вебера
        private struct ParamPotential
        {
            public double A, B, p, a, l, y;
            public ParamPotential(double A, double B, double p, double a, double l, double y)
            {
                this.A = A;
                this.B = B;
                this.p = p;
                this.a = a;
                this.l = l;
                this.y = y;
            }
            /*
            public ParamPotential(ParamPotential potential1, ParamPotential potential2)
            {
                this.A = Math.Sqrt(potential1.A * potential2.A);
                this.B = Math.Sqrt(potential1.B * potential2.B);
                this.p = Math.Sqrt(potential1.p * potential2.p);
                this.a = Math.Sqrt(potential1.a * potential2.a);
                this.l = 0.5 * (potential1.l + potential2.l);
                this.y = 0.5 * (potential1.y + potential2.y);
            }*/
        }
        //Параметры для различных типов атомов
        private ParamPotential STW_Si, STW_Ge, STW_SiGe;

        public StillingerWeber()
        {
            STW_Si = new ParamPotential(7.0496, 0.6022, 4, 1.80, 21.0, 1.20);
            STW_Ge = new ParamPotential(7.0496, 0.6022, 4, 1.80, 31.0, 1.20);
            STW_SiGe = new ParamPotential(7.0496, 0.6022, 4, 1.80, 24.333, 1.20);
        }
        /// <summary>
        /// Функция двухчастичного взаимодействия ф(r)
        /// </summary>
        /// <param name="potential">Параметры потенциала</param>
        /// <param name="radius">Расстояние до атома-соседа</param>
        /// <returns></returns>
        private double P_FunctionCutoff(ParamPotential potential, double radius)
        {
            if (radius >= potential.a) return 0;
            else return potential.A * (potential.B * Math.Pow(radius, -potential.p) - 1) * Math.Exp(Math.Pow(radius - potential.a, -1));
        }
        /// <summary>
        /// Затухающая функция, критический радиус которой находится между первым и вторым ближайшим соседом
        /// </summary>
        /// <param name="potential">Параметры потенциала</param>
        /// <param name="Rij">Расстояние до первого атома-соседа</param>
        /// <param name="Rik">Расстояние до второго атома-соседа</param>
        /// <returns></returns>
        private double P_FunctionH(ParamPotential potential, double Rij, double Rik)
        {
            if (Rij >= potential.a) return 0;
            else if (Rik >= potential.a) return 0;
            else
            {
                double exponenta = potential.y * Math.Pow(Rij - potential.a, -1) + potential.y * Math.Pow(Rik - potential.a, -1);
                return potential.l * Math.Exp(exponenta);
            }
        }
        /// <summary>
        /// Функция (cos + 1/3)^2
        /// </summary>
        /// <param name="Rij">Расстояние до первого атома-соседа</param>
        /// <param name="Rik">Расстояние до второго атома-соседа</param>
        /// <param name="Rjk">Расстояние между первым атомом-соседом и вторым атомом-соседом</param>
        /// <returns></returns>
        private double P_FunctionCos(double Rij, double Rik, double Rjk)
        {
            double cosTeta = (Math.Pow(Rik, 2) + Math.Pow(Rij, 2) - Math.Pow(Rjk, 2)) / (2 * Rij * Rik);
            return Math.Pow(cosTeta + (1 / 3d), 2);
        }
        /// <summary>
        /// Потенциальная энергия выбранного атома
        /// </summary>
        /// <param name="selAtom">Выбранный атом</param>
        /// <param name="indexSelAtom">Индекс выбранного атома в ячейке</param>
        /// <param name="atomNeighbours">Атомы-соседи</param>
        /// <param name="atomTypeNeigbours">Типы атомов-соседей</param>
        /// <returns></returns>
        public override double PotentialEnergy(Atom selAtom, int indexSelAtom, ref Atom[] atomNeighbours)
        {
            double atomEnergy = 0;

            for (int j = 0; j < atomNeighbours.Length; j++)
            {
                double Rij = Vector.Magnitude(selAtom.GlobalCoord, atomNeighbours[j].AtomCoord);

                //В зависимости от типов атомов меняются параметры потенциала
                ParamPotential potential = STW_SiGe;
                if (selAtom.atomType == AtomType.Si && selAtom.atomType == atomNeighbours[j].atomType) potential = STW_Si;
                if (selAtom.atomType == AtomType.Ge && selAtom.atomType == atomNeighbours[j].atomType) potential = STW_Ge;

                atomEnergy += 0.5 * P_FunctionCutoff(potential, Rij);

                for (int k = 0; k < atomNeighbours.Length; k++)
                {
                    if (k == j) continue;
                    double Rik = Vector.Magnitude(selAtom.GlobalCoord, atomNeighbours[k].AtomCoord);
                    double Rjk = Vector.Magnitude(atomNeighbours[j].AtomCoord, atomNeighbours[k].AtomCoord);

                    atomEnergy += (P_FunctionH(potential, Rij, Rik) + P_FunctionH(potential, Rij, Rjk) + P_FunctionH(potential, Rik, Rjk)) * P_FunctionCos(Rij, Rik, Rjk);
                }
            }

            return atomEnergy;
        }
    }
}
