﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Segregation
{
    /// <summary>
    /// Тип атома
    /// </summary>
    public enum AtomType
    {
        Si, //Silicium
        Ge //Germanium
    }

    public struct Atom
    {
        /// <summary>
        /// Параметр решётки
        /// </summary>
        public static double LatPar = 1;//Lattice parameter

        //Тип атома
        public AtomType atomType;
        //Координата атома
        private Vector atomCoord;
        //Скорость атома
        public Vector atomVeloc;
        //Позиция ячейки в которой находится атом
        public Vector cellCoord;

        /// <summary>
        /// Координата атома с учётом ячейки, в которой он размещён
        /// </summary>
        public Vector GlobalCoord
        {
            get { return atomCoord + cellCoord; }
        }
        /// <summary>
        /// Изменение координаты атома с применением периодических граничных условий
        /// </summary>
        public Vector AtomCoord
        {
            set
            {
                if (value.x > LatPar) atomCoord.x = value.x % LatPar;
                else if (value.x < 0) atomCoord.x = LatPar + (value.x % LatPar);
                else atomCoord.x = value.x;

                if (value.y > LatPar) atomCoord.y = value.y % LatPar;
                else if (value.y < 0) atomCoord.y = LatPar + (value.y % LatPar);
                else atomCoord.y = value.y;

                if (value.z > LatPar) atomCoord.z = value.z % LatPar;
                else if (value.z < 0) atomCoord.z = LatPar + (value.z % LatPar);
                else atomCoord.z = value.z;
            }

            get
            {
                return atomCoord;
            }
        }

        public Vector SetCoord
        {
            set { atomCoord = value; }
        }
    }
}
