﻿namespace Segregation
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.PictureBox pictureBox1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.TabControl tabControl1;
            System.Windows.Forms.PictureBox pictureBox2;
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button_Calculated = new System.Windows.Forms.Button();
            this.textBox_SumEnergy = new System.Windows.Forms.TextBox();
            this.numericUpDown_Size = new System.Windows.Forms.NumericUpDown();
            this.radioButton_TRS = new System.Windows.Forms.RadioButton();
            this.radioButton_STW = new System.Windows.Forms.RadioButton();
            this.button_Close = new System.Windows.Forms.Button();
            this.numUD_N = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            pictureBox1 = new System.Windows.Forms.PictureBox();
            tabControl1 = new System.Windows.Forms.TabControl();
            pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(pictureBox1)).BeginInit();
            tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Size)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_N)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(16, 47);
            label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(131, 17);
            label1.TabIndex = 2;
            label1.Text = "Энергия системы -";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(16, 11);
            label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(126, 17);
            label2.TabIndex = 4;
            label2.Text = "Размер системы -";
            // 
            // pictureBox1
            // 
            pictureBox1.BackColor = System.Drawing.Color.White;
            pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            pictureBox1.Location = new System.Drawing.Point(8, 7);
            pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new System.Drawing.Size(627, 308);
            pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            pictureBox1.TabIndex = 6;
            pictureBox1.TabStop = false;
            // 
            // tabControl1
            // 
            tabControl1.Controls.Add(this.tabPage1);
            tabControl1.Controls.Add(this.tabPage2);
            tabControl1.Location = new System.Drawing.Point(16, 76);
            tabControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            tabControl1.Name = "tabControl1";
            tabControl1.SelectedIndex = 0;
            tabControl1.Size = new System.Drawing.Size(653, 354);
            tabControl1.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(pictureBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Size = new System.Drawing.Size(645, 325);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Потенциал Терсоффа";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(pictureBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage2.Size = new System.Drawing.Size(645, 325);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Потенциал Стиллинджера-Вебера";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pictureBox2
            // 
            pictureBox2.BackColor = System.Drawing.Color.White;
            pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            pictureBox2.Location = new System.Drawing.Point(8, 47);
            pictureBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            pictureBox2.Name = "pictureBox2";
            pictureBox2.Size = new System.Drawing.Size(627, 223);
            pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            pictureBox2.TabIndex = 7;
            pictureBox2.TabStop = false;
            // 
            // button_Calculated
            // 
            this.button_Calculated.BackColor = System.Drawing.Color.White;
            this.button_Calculated.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Calculated.Location = new System.Drawing.Point(161, 41);
            this.button_Calculated.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_Calculated.Name = "button_Calculated";
            this.button_Calculated.Size = new System.Drawing.Size(100, 28);
            this.button_Calculated.TabIndex = 0;
            this.button_Calculated.Text = "Подсчёт";
            this.button_Calculated.UseVisualStyleBackColor = false;
            this.button_Calculated.Click += new System.EventHandler(this.button_Calculated_Click);
            // 
            // textBox_SumEnergy
            // 
            this.textBox_SumEnergy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_SumEnergy.Location = new System.Drawing.Point(287, 44);
            this.textBox_SumEnergy.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox_SumEnergy.Name = "textBox_SumEnergy";
            this.textBox_SumEnergy.Size = new System.Drawing.Size(209, 22);
            this.textBox_SumEnergy.TabIndex = 1;
            // 
            // numericUpDown_Size
            // 
            this.numericUpDown_Size.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown_Size.Location = new System.Drawing.Point(161, 9);
            this.numericUpDown_Size.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numericUpDown_Size.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDown_Size.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDown_Size.Name = "numericUpDown_Size";
            this.numericUpDown_Size.Size = new System.Drawing.Size(67, 22);
            this.numericUpDown_Size.TabIndex = 3;
            this.numericUpDown_Size.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // radioButton_TRS
            // 
            this.radioButton_TRS.AutoSize = true;
            this.radioButton_TRS.Checked = true;
            this.radioButton_TRS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_TRS.Location = new System.Drawing.Point(338, 11);
            this.radioButton_TRS.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton_TRS.Name = "radioButton_TRS";
            this.radioButton_TRS.Size = new System.Drawing.Size(73, 21);
            this.radioButton_TRS.TabIndex = 8;
            this.radioButton_TRS.TabStop = true;
            this.radioButton_TRS.Text = "Tersoff";
            this.radioButton_TRS.UseVisualStyleBackColor = true;
            // 
            // radioButton_STW
            // 
            this.radioButton_STW.AutoSize = true;
            this.radioButton_STW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_STW.Location = new System.Drawing.Point(423, 11);
            this.radioButton_STW.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton_STW.Name = "radioButton_STW";
            this.radioButton_STW.Size = new System.Drawing.Size(124, 21);
            this.radioButton_STW.TabIndex = 9;
            this.radioButton_STW.Text = "StillingerWeber";
            this.radioButton_STW.UseVisualStyleBackColor = true;
            // 
            // button_Close
            // 
            this.button_Close.BackColor = System.Drawing.Color.White;
            this.button_Close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Close.Location = new System.Drawing.Point(555, 15);
            this.button_Close.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(115, 54);
            this.button_Close.TabIndex = 10;
            this.button_Close.Text = "Выход";
            this.button_Close.UseVisualStyleBackColor = false;
            this.button_Close.Click += new System.EventHandler(this.button_Close_Click);
            // 
            // numUD_N
            // 
            this.numUD_N.DecimalPlaces = 2;
            this.numUD_N.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numUD_N.Location = new System.Drawing.Point(270, 9);
            this.numUD_N.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUD_N.Name = "numUD_N";
            this.numUD_N.Size = new System.Drawing.Size(61, 22);
            this.numUD_N.TabIndex = 11;
            this.numUD_N.Value = new decimal(new int[] {
            3,
            0,
            0,
            65536});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(237, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "GE";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 434);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numUD_N);
            this.Controls.Add(this.button_Close);
            this.Controls.Add(this.radioButton_STW);
            this.Controls.Add(this.radioButton_TRS);
            this.Controls.Add(tabControl1);
            this.Controls.Add(label2);
            this.Controls.Add(this.numericUpDown_Size);
            this.Controls.Add(label1);
            this.Controls.Add(this.textBox_SumEnergy);
            this.Controls.Add(this.button_Calculated);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(701, 481);
            this.MinimumSize = new System.Drawing.Size(701, 112);
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Сегрегация SiGe";
            ((System.ComponentModel.ISupportInitialize)(pictureBox1)).EndInit();
            tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Size)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_N)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Calculated;
        private System.Windows.Forms.TextBox textBox_SumEnergy;
        private System.Windows.Forms.NumericUpDown numericUpDown_Size;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.RadioButton radioButton_TRS;
        private System.Windows.Forms.RadioButton radioButton_STW;
        private System.Windows.Forms.Button button_Close;
        private System.Windows.Forms.NumericUpDown numUD_N;
        private System.Windows.Forms.Label label3;
    }
}

