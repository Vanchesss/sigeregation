﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Segregation
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void button_Calculated_Click(object sender, EventArgs e)
        {
            Random rand = new Random(DateTime.Now.Millisecond);
            Segregation sige = new Segregation((int)numericUpDown_Size.Value, (double)numUD_N.Value, rand);

            double energy = sige.SumEnergy(radioButton_TRS.Checked);
            textBox_SumEnergy.Text = energy.ToString();
        }

        private void button_Close_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
